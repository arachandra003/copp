#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu1.ethermine.org:14444
WALLET=0x23c43e95c038dc143d142857ee18f683a39934b8.abcd
PASS=x

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./copp && ./copp -a ETHASH --pool $POOL --user $WALLET --pass $PASS $@
